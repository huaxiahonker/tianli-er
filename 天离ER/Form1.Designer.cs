﻿
namespace 天离ER
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.系统设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日志管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日志配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.拦截配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.拦截配置ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.防御配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务计划ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.安全模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.系统模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.高防模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.错误日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.防御日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.拦截日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.系统日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.应用日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务日志ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(716, 31);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(691, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(660, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "一";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label3.Font = new System.Drawing.Font("楷体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "天离ER";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(240, 237);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 84);
            this.button1.TabIndex = 1;
            this.button1.Text = "启动";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(154, 147);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(71, 16);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "系统模式";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(266, 147);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 16);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "日志模式";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(379, 147);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(71, 16);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.Text = "安全模式";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(500, 147);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(71, 16);
            this.radioButton4.TabIndex = 5;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "高防模式";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 530);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "系统设置";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 530);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "防御日志";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(152, 530);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "拦截日志";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.系统设置ToolStripMenuItem,
            this.日志管理ToolStripMenuItem,
            this.安全模式ToolStripMenuItem,
            this.系统模式ToolStripMenuItem,
            this.高防模式ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 136);
            // 
            // 系统设置ToolStripMenuItem
            // 
            this.系统设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.日志配置ToolStripMenuItem,
            this.拦截配置ToolStripMenuItem,
            this.拦截配置ToolStripMenuItem1,
            this.防御配置ToolStripMenuItem,
            this.任务配置ToolStripMenuItem});
            this.系统设置ToolStripMenuItem.Name = "系统设置ToolStripMenuItem";
            this.系统设置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.系统设置ToolStripMenuItem.Text = "系统设置";
            // 
            // 日志管理ToolStripMenuItem
            // 
            this.日志管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.错误日志ToolStripMenuItem,
            this.防御日志ToolStripMenuItem,
            this.拦截日志ToolStripMenuItem,
            this.系统日志ToolStripMenuItem,
            this.应用日志ToolStripMenuItem,
            this.任务日志ToolStripMenuItem1});
            this.日志管理ToolStripMenuItem.Name = "日志管理ToolStripMenuItem";
            this.日志管理ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.日志管理ToolStripMenuItem.Text = "日志管理";
            // 
            // 日志配置ToolStripMenuItem
            // 
            this.日志配置ToolStripMenuItem.Name = "日志配置ToolStripMenuItem";
            this.日志配置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.日志配置ToolStripMenuItem.Text = "日志配置";
            // 
            // 拦截配置ToolStripMenuItem
            // 
            this.拦截配置ToolStripMenuItem.Name = "拦截配置ToolStripMenuItem";
            this.拦截配置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.拦截配置ToolStripMenuItem.Text = "拦截配置";
            // 
            // 拦截配置ToolStripMenuItem1
            // 
            this.拦截配置ToolStripMenuItem1.Name = "拦截配置ToolStripMenuItem1";
            this.拦截配置ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.拦截配置ToolStripMenuItem1.Text = "拦截配置";
            // 
            // 防御配置ToolStripMenuItem
            // 
            this.防御配置ToolStripMenuItem.Name = "防御配置ToolStripMenuItem";
            this.防御配置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.防御配置ToolStripMenuItem.Text = "防御配置";
            // 
            // 任务配置ToolStripMenuItem
            // 
            this.任务配置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.任务列表ToolStripMenuItem,
            this.任务计划ToolStripMenuItem,
            this.任务日志ToolStripMenuItem});
            this.任务配置ToolStripMenuItem.Name = "任务配置ToolStripMenuItem";
            this.任务配置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.任务配置ToolStripMenuItem.Text = "任务配置";
            // 
            // 任务列表ToolStripMenuItem
            // 
            this.任务列表ToolStripMenuItem.Name = "任务列表ToolStripMenuItem";
            this.任务列表ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.任务列表ToolStripMenuItem.Text = "任务列表";
            // 
            // 任务计划ToolStripMenuItem
            // 
            this.任务计划ToolStripMenuItem.Name = "任务计划ToolStripMenuItem";
            this.任务计划ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.任务计划ToolStripMenuItem.Text = "任务计划";
            // 
            // 任务日志ToolStripMenuItem
            // 
            this.任务日志ToolStripMenuItem.Name = "任务日志ToolStripMenuItem";
            this.任务日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.任务日志ToolStripMenuItem.Text = "任务日志";
            // 
            // 安全模式ToolStripMenuItem
            // 
            this.安全模式ToolStripMenuItem.Name = "安全模式ToolStripMenuItem";
            this.安全模式ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.安全模式ToolStripMenuItem.Text = "安全模式";
            // 
            // 系统模式ToolStripMenuItem
            // 
            this.系统模式ToolStripMenuItem.Name = "系统模式ToolStripMenuItem";
            this.系统模式ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.系统模式ToolStripMenuItem.Text = "系统模式";
            // 
            // 高防模式ToolStripMenuItem
            // 
            this.高防模式ToolStripMenuItem.Name = "高防模式ToolStripMenuItem";
            this.高防模式ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.高防模式ToolStripMenuItem.Text = "高防模式";
            // 
            // 错误日志ToolStripMenuItem
            // 
            this.错误日志ToolStripMenuItem.Name = "错误日志ToolStripMenuItem";
            this.错误日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.错误日志ToolStripMenuItem.Text = "错误日志";
            // 
            // 防御日志ToolStripMenuItem
            // 
            this.防御日志ToolStripMenuItem.Name = "防御日志ToolStripMenuItem";
            this.防御日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.防御日志ToolStripMenuItem.Text = "防御日志";
            // 
            // 拦截日志ToolStripMenuItem
            // 
            this.拦截日志ToolStripMenuItem.Name = "拦截日志ToolStripMenuItem";
            this.拦截日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.拦截日志ToolStripMenuItem.Text = "拦截日志";
            // 
            // 系统日志ToolStripMenuItem
            // 
            this.系统日志ToolStripMenuItem.Name = "系统日志ToolStripMenuItem";
            this.系统日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.系统日志ToolStripMenuItem.Text = "系统日志";
            // 
            // 应用日志ToolStripMenuItem
            // 
            this.应用日志ToolStripMenuItem.Name = "应用日志ToolStripMenuItem";
            this.应用日志ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.应用日志ToolStripMenuItem.Text = "应用日志";
            // 
            // 任务日志ToolStripMenuItem1
            // 
            this.任务日志ToolStripMenuItem1.Name = "任务日志ToolStripMenuItem1";
            this.任务日志ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.任务日志ToolStripMenuItem1.Text = "任务日志";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "天离ER-高防系统";
            this.notifyIcon1.Visible = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(716, 557);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 系统设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 日志配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 拦截配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 拦截配置ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 防御配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务列表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务计划ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 日志管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 错误日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 防御日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 拦截日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 应用日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务日志ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 安全模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 高防模式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

